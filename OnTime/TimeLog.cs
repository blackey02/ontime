﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OnTime
{
    public class TimeLog
    {
        public Guid ID;
        public DateTime StartTime;
        public DateTime EndTime;
        public string UserId;
        public string Project;
        public List<string> Notes;

        public TimeLog()
        {
            ID = Guid.Empty;
            Notes = new List<string>();
        }
    }

    // Compare timelogs by project
    public class ProjectComparer : IEqualityComparer<TimeLog>
    {
        #region IEqualityComparer<TimeLog> Members
        public bool Equals(TimeLog x, TimeLog y)
        {
            return x.Project.Equals(y.Project);
        }

        public int GetHashCode(TimeLog obj)
        {
            return obj.Project.GetHashCode();
        }
        #endregion
    }

    // Compare timelogs by StartTime
    public class StartTimeComparer : IEqualityComparer<TimeLog>
    {
        #region IEqualityComparer<TimeLog> Members
        public bool Equals(TimeLog x, TimeLog y)
        {
            return x.StartTime.Date.Equals(y.StartTime.Date);
        }

        public int GetHashCode(TimeLog obj)
        {
            return obj.StartTime.Date.GetHashCode();
        }
        #endregion
    }
}
