﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Web;

namespace OnTime
{
    public partial class NoteForm : Form
    {
        public string Note { get; set; }

        public NoteForm()
        {
            InitializeComponent();
        }

        private void buttonAddNote_Click(object sender, EventArgs e)
        {
            Note = richTextBoxNote.Text;
            this.Close();
        }
    }
}
