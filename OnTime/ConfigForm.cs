﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Amazon;
using Amazon.SimpleDB;
using Amazon.SimpleDB.Model;

namespace OnTime
{
    public partial class ConfigForm : Form
    {
        public event Action<string> ProjectAdded;
        public event Action<string> ProjectRemoved;
        public event EventHandler ProjectsCleared;

        public ConfigForm()
        {
            InitializeComponent();
        }

        private void ConfigForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Hide();
            e.Cancel = true;
        }

        public void SetEnabled(bool enabled)
        {
            buttonAddProject.Enabled = enabled;
            buttonRefreshUser.Enabled = enabled;
            buttonRemoveProject.Enabled = enabled;
        }

        private void buttonRemoveProject_Click(object sender, EventArgs e)
        {
            int selectedIdx = listBoxProjects.SelectedIndex;
            string selectedItem = (string)listBoxProjects.SelectedItem;
            if (selectedIdx != -1)
            {
                bool success = AppContext.Database.RemoveProject(selectedItem);
                if (success)
                {
                    listBoxProjects.Items.RemoveAt(selectedIdx);
                    FireProjectRemoved(selectedItem);
                }
            }
        }

        private void buttonAddProject_Click(object sender, EventArgs e)
        {
            string newProject = textBoxProjectName.Text;
            if (!newProject.IsNullOrWhitespace())
            {
                newProject = newProject.Trim();
                bool foundExisting = false;
                foreach (var proj in listBoxProjects.Items)
                {
                    if (proj.ToString().ToLowerInvariant() == newProject.ToLowerInvariant())
                    {
                        foundExisting = true;
                        break;
                    }
                }

                if (foundExisting)
                    MessageBox.Show(string.Format("It looks like that project already exists"));
                else
                {
                    textBoxProjectName.Text = string.Empty;
                    bool success = AppContext.Database.AddProject(newProject);
                    if (success)
                    {
                        listBoxProjects.Items.Add(newProject);
                        FireProjectAdded(newProject);
                    }
                }
            }
        }

        private void FireProjectAdded(string project)
        {
            if(ProjectAdded != null)
                ProjectAdded(project);
        }

        private void FireProjectRemoved(string project)
        {
            if(ProjectRemoved != null)
                ProjectRemoved(project);
        }

        private void FireProjectsCleared()
        {
            if (ProjectsCleared != null)
                ProjectsCleared(this, EventArgs.Empty);
        }

        private void textBoxFullName_VisibleChanged(object sender, EventArgs e)
        {
            if (this.Visible)
                ReloadProjects();
        }

        private void buttonRefreshUser_Click(object sender, EventArgs e)
        {
            buttonRefreshUser.Text = "Logging In";
            ReloadProjects();
            buttonRefreshUser.Text = AppContext.Database.UserId.IsNullOrWhitespace() ? "Logged Out" : "Logged In";
        }

        private void ReloadProjects()
        {
            listBoxProjects.Items.Clear();
            FireProjectsCleared();

            AppContext.Database.UserId = textBoxFullName.Text;
            var projects = AppContext.Database.GetProjects();
            projects.ForEach(new Action<string>(project => { listBoxProjects.Items.Add(project); FireProjectAdded(project); }));
        }
    }
}
