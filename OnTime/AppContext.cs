﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;

namespace OnTime
{
    class AppContext : ApplicationContext
    {
        private static AWSDatabase _awsDb = new AWSDatabase();
        private NotifyIcon _notifyIcon = new NotifyIcon();
        private MenuItem _projectMenuItm = new MenuItem("Projects");
        private ConfigForm _configForm = new ConfigForm();
        private TimeLog _currentTimeLog = new TimeLog();
        private Visualizer _visualizer = new Visualizer();

        public AppContext()
        {
            _configForm.ProjectAdded += new Action<string>(configForm_ProjectAdded);
            _configForm.ProjectRemoved += new Action<string>(configForm_ProjectRemoved);
            _configForm.ProjectsCleared += new EventHandler(configForm_ProjectsCleared);
            _visualizer.StopClicked += StopLogging;
            _visualizer.ProjectSelected += new Action<object, string>(visualizer_ProjectSelected);
            _visualizer.ShowNoteWindow += AddNote;

            var configMenuItem = new MenuItem("Configuration", new EventHandler(ShowConfig));
            var visualizer = new MenuItem("Visualizer", new EventHandler(ShowVisualizer));
            var noteMenuItem = new MenuItem("Add Note", new EventHandler(AddNote));
            var reportMenuItem = new MenuItem("Create Report", new EventHandler(CreateReport));
            var stopMenuItem = new MenuItem("Stop", new EventHandler(StopLogging));
            var exitMenuItem = new MenuItem("Exit", new EventHandler(Exit));

            _notifyIcon.Icon = Properties.Resources.time;
            _notifyIcon.ContextMenu = new ContextMenu(new MenuItem[] { configMenuItem, visualizer, _projectMenuItm, noteMenuItem, reportMenuItem, stopMenuItem, exitMenuItem });
            _notifyIcon.Visible = true;

            UpdateStatusBarContext();
            _awsDb.InitFirstConnection();
        }

        private void visualizer_ProjectSelected(object arg1, string arg2)
        {
            ProjectClicked(arg2);
        }

        private void UpdateStatusBarContext()
        {
            bool userIsValid = !_awsDb.UserId.IsNullOrWhitespace();

            if (Guid.Equals(_currentTimeLog.ID, default(Guid)) || !userIsValid)
            {
                _configForm.SetEnabled(true);

                foreach (MenuItem item in _notifyIcon.ContextMenu.MenuItems)
                {
                    switch (item.Text)
                    {
                        case "Add Note":
                        case "Stop":
                            item.Enabled = false;
                            break;
                        case "Configuration":
                            item.Enabled = true;
                            break;
                        case "Create Report":
                            item.Enabled = userIsValid;
                            break;
                        default:
                            break;
                    }
                }
            }
            else
            {
                _configForm.SetEnabled(false);

                foreach (MenuItem item in _notifyIcon.ContextMenu.MenuItems)
                {
                    switch (item.Text)
                    {
                        case "Create Report":
                        case "Add Note":
                        case "Stop":
                            item.Enabled = true; ;
                            break;
                        case "Configuration":
                            item.Enabled = false;
                            break;
                        default:
                            break;
                    }
                }
            }
        }

        private void configForm_ProjectsCleared(object sender, EventArgs e)
        {
            _projectMenuItm.MenuItems.Clear();
            _visualizer.ProjectsCleared();
            UpdateStatusBarContext();
        }

        private void configForm_ProjectRemoved(string project)
        {
            MenuItem item = null;
            foreach (MenuItem mItem in _projectMenuItm.MenuItems)
            {
                if (mItem.Text == project)
                {
                    item = mItem;
                    break;
                }
            }

            if (item != null)
            {
                _projectMenuItm.MenuItems.Remove(item);
                _visualizer.ProjectRemoved(project);
                _awsDb.RemoveTimeLogs(project);
                UpdateStatusBarContext();
            }
        }

        private void configForm_ProjectAdded(string project)
        {
            var newProjectItem = new MenuItem(project, new EventHandler((obj, obj2) => ProjectClicked(project)));
            _projectMenuItm.MenuItems.Add(newProjectItem);
            _visualizer.ProjectAdded(project);
            UpdateStatusBarContext();
        }

        private void ProjectClicked(string project)
        {
            if (Guid.Equals(_currentTimeLog.ID, default(Guid)))
            {
                _currentTimeLog.ID = Guid.NewGuid();
                _currentTimeLog.StartTime = DateTime.UtcNow;
                _currentTimeLog.Project = project;
            }
            else
            {
                _currentTimeLog.UserId = _awsDb.UserId;
                _currentTimeLog.EndTime = DateTime.UtcNow;
                _awsDb.AddTimeLog(_currentTimeLog);

                _currentTimeLog = new TimeLog();
                _currentTimeLog.ID = Guid.NewGuid();
                _currentTimeLog.StartTime = DateTime.UtcNow;
                _currentTimeLog.Project = project;
            }
            _visualizer.AddProject(_currentTimeLog);
            UpdateStatusBarContext();
        }

        public static AWSDatabase Database
        {
            get
            {
                return _awsDb;
            }
        }

        private void StopLogging(object sender, EventArgs e)
        {
            if (!Guid.Equals(_currentTimeLog.ID, default(Guid)))
            {
                _visualizer.Stop();
                _currentTimeLog.UserId = _awsDb.UserId;
                _currentTimeLog.EndTime = DateTime.UtcNow;
                _awsDb.AddTimeLog(_currentTimeLog);
                _currentTimeLog = new TimeLog();
            }
            UpdateStatusBarContext();
        }

        private void AddNote(object sender, EventArgs e)
        { 
            string note = string.Empty;
            var noteForm = new NoteForm();
            noteForm.ShowDialog();
            note = noteForm.Note;
            noteForm.Dispose();
            _currentTimeLog.Notes.Add(note);
        }

        private void CreateReport(object sender, EventArgs e)
        {
            Reporter.Instance.CreateReport(null, DateTime.Now.AddDays(-30), DateTime.Now);
        }

        private void ShowConfig(object sender, EventArgs e)
        {
            _configForm.Show();
        }

        private void ShowVisualizer(object sender, EventArgs e)
        {
            _visualizer.Show();
        }

        private void Exit(object sender, EventArgs e)
        {
            StopLogging(this, EventArgs.Empty);
            _awsDb.Dispose();
            _notifyIcon.Visible = false;
            this.ExitThread();
        }
    }
}
