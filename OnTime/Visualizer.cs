﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace OnTime
{
    public partial class Visualizer : Form
    {
        public event EventHandler StopClicked;
        public event Action<object, string> ProjectSelected;
        public event EventHandler ShowNoteWindow;

        private string _activeItemId = string.Empty;

        public Visualizer()
        {
            InitializeComponent();

            ShowSelectButton(true);
        }

        private void Visualizer_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
            this.Hide();
        }

        public void ProjectAdded(string project)
        {
            comboBoxProjects.Items.Add(project);
        }

        public void ProjectRemoved(string project)
        {
            comboBoxProjects.Items.Remove(project);
        }

        public void ProjectsCleared()
        {
            comboBoxProjects.Items.Clear();
        }

        private void FireStopClicked()
        {
            if (StopClicked != null)
                StopClicked(this, EventArgs.Empty);
        }

        private void FireProjectSelected()
        {
            if (ProjectSelected != null)
                ProjectSelected(this, (string)comboBoxProjects.SelectedItem);
        }

        private void FireShowNoteWindow()
        {
            if (ShowNoteWindow != null)
                ShowNoteWindow(this, EventArgs.Empty);
        }

        private void ShowSelectButton(bool showSelect)
        {
            if (showSelect)
            {
                buttonAddNote.Hide();
                buttonSelect.Show();
            }
            else
            {
                buttonSelect.Hide();
                buttonAddNote.Show();
            }
        }

        public void AddProject(TimeLog timeLog)
        {
            Stop();

            if (!Guid.Equals(timeLog.ID, default(Guid)))
            {
                ShowSelectButton(false);

                VisualizerItem vItem = null;
                foreach (Control ctrl in flowLayoutPanelMain.Controls)
                {
                    VisualizerItem vCtrl = ctrl as VisualizerItem;
                    if (vCtrl != null && vCtrl.ProjectName.Equals(timeLog.Project))
                        vItem = vCtrl;
                }

                if (vItem == null)
                {
                    vItem = new VisualizerItem(timeLog.Project);
                    _activeItemId = vItem.Tag as string;
                    vItem.StopClicked += new EventHandler((obj1, obj2) => { Stop(); FireStopClicked(); });
                    flowLayoutPanelMain.Controls.Add(vItem);
                }
                else
                {
                    _activeItemId = (string)vItem.Tag;
                    vItem.ReActivate();
                }
                
                flowLayoutPanelMain.ScrollControlIntoView(vItem);
            }
        }

        // We were told to stop
        public void Stop()
        {
            if (_activeItemId.IsNullOrWhitespace())
                return;

            ShowSelectButton(true);
            foreach (Control ctrlItem in flowLayoutPanelMain.Controls)
            {
                if (ctrlItem.Tag.Equals(_activeItemId))
                {
                    var visualizerItem = ctrlItem as VisualizerItem;
                    if (visualizerItem != null)
                        visualizerItem.Stop();
                    break;
                }
            }
            _activeItemId = string.Empty;
        }

        private void buttonClear_Click(object sender, EventArgs e)
        {
            Stop();
            flowLayoutPanelMain.Controls.Clear();
            FireStopClicked();
        }

        private void buttonSelect_Click(object sender, EventArgs e)
        {
            string item = (string)comboBoxProjects.SelectedItem;
            if (!item.IsNullOrWhitespace())
                FireProjectSelected();
        }

        private void buttonAddNote_Click(object sender, EventArgs e)
        {
            FireShowNoteWindow();
        }
    }
}
