﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OnTime
{
    public static class Extensions
    {
        public static bool IsNullOrWhitespace(this string value)
        { 
            bool isNullOrWhitespace = true;
            if (!string.IsNullOrEmpty(value))
            {
                var trimmed = value.Trim();
                if (!string.IsNullOrEmpty(trimmed))
                    isNullOrWhitespace = false;
            }
            return isNullOrWhitespace;
        }

        public static int ToUnixTime(this DateTime value)
        {
            // Unix timestamp is seconds past epoch
            var epoch = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            int unixTime = Convert.ToInt32((value - epoch).TotalSeconds);
            return unixTime;
        }

        public static DateTime FromUnixTime(this int value)
        {
            var epoch = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            var timeSpan = new TimeSpan(0, 0, value);
            var timeStamp = epoch + timeSpan;
            return timeStamp;
        }
    }
}
