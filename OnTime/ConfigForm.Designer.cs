﻿namespace OnTime
{
    partial class ConfigForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ConfigForm));
            this.listBoxProjects = new System.Windows.Forms.ListBox();
            this.textBoxProjectName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.buttonAddProject = new System.Windows.Forms.Button();
            this.buttonRemoveProject = new System.Windows.Forms.Button();
            this.textBoxFullName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.buttonRefreshUser = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // listBoxProjects
            // 
            this.listBoxProjects.FormattingEnabled = true;
            this.listBoxProjects.Location = new System.Drawing.Point(11, 107);
            this.listBoxProjects.Name = "listBoxProjects";
            this.listBoxProjects.Size = new System.Drawing.Size(233, 121);
            this.listBoxProjects.Sorted = true;
            this.listBoxProjects.TabIndex = 0;
            // 
            // textBoxProjectName
            // 
            this.textBoxProjectName.Location = new System.Drawing.Point(12, 80);
            this.textBoxProjectName.Name = "textBoxProjectName";
            this.textBoxProjectName.Size = new System.Drawing.Size(171, 20);
            this.textBoxProjectName.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 64);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(40, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Project";
            // 
            // buttonAddProject
            // 
            this.buttonAddProject.Location = new System.Drawing.Point(189, 78);
            this.buttonAddProject.Name = "buttonAddProject";
            this.buttonAddProject.Size = new System.Drawing.Size(55, 23);
            this.buttonAddProject.TabIndex = 3;
            this.buttonAddProject.Text = "Add";
            this.buttonAddProject.UseVisualStyleBackColor = true;
            this.buttonAddProject.Click += new System.EventHandler(this.buttonAddProject_Click);
            // 
            // buttonRemoveProject
            // 
            this.buttonRemoveProject.Location = new System.Drawing.Point(181, 234);
            this.buttonRemoveProject.Name = "buttonRemoveProject";
            this.buttonRemoveProject.Size = new System.Drawing.Size(63, 23);
            this.buttonRemoveProject.TabIndex = 4;
            this.buttonRemoveProject.Text = "Remove";
            this.buttonRemoveProject.UseVisualStyleBackColor = true;
            this.buttonRemoveProject.Click += new System.EventHandler(this.buttonRemoveProject_Click);
            // 
            // textBoxFullName
            // 
            this.textBoxFullName.Location = new System.Drawing.Point(12, 30);
            this.textBoxFullName.Name = "textBoxFullName";
            this.textBoxFullName.Size = new System.Drawing.Size(134, 20);
            this.textBoxFullName.TabIndex = 5;
            this.textBoxFullName.VisibleChanged += new System.EventHandler(this.textBoxFullName_VisibleChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 14);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(54, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Full Name";
            // 
            // buttonRefreshUser
            // 
            this.buttonRefreshUser.Location = new System.Drawing.Point(152, 28);
            this.buttonRefreshUser.Name = "buttonRefreshUser";
            this.buttonRefreshUser.Size = new System.Drawing.Size(92, 23);
            this.buttonRefreshUser.TabIndex = 7;
            this.buttonRefreshUser.Text = "Logged Out";
            this.buttonRefreshUser.UseVisualStyleBackColor = true;
            this.buttonRefreshUser.Click += new System.EventHandler(this.buttonRefreshUser_Click);
            // 
            // ConfigForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(256, 266);
            this.Controls.Add(this.buttonRefreshUser);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textBoxFullName);
            this.Controls.Add(this.buttonRemoveProject);
            this.Controls.Add(this.buttonAddProject);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBoxProjectName);
            this.Controls.Add(this.listBoxProjects);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ConfigForm";
            this.Text = "Configuration";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ConfigForm_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox listBoxProjects;
        private System.Windows.Forms.TextBox textBoxProjectName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button buttonAddProject;
        private System.Windows.Forms.Button buttonRemoveProject;
        private System.Windows.Forms.TextBox textBoxFullName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button buttonRefreshUser;

    }
}

