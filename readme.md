#OnTime

##Last Update
Years Ago

##Summary
An application to keep track of the time spent on the projects that matter most.

This application lets you keep track of all your time by simply selecting a project each time you change focus, then at the end of the day, or month you can generate a report with big beautiful graphs.

The application uses AWS SimpleDb so you'll need to input your AWS accessKey and secretAccessKey. You'll need to create 'Projects' and 'TimeLogs' domains.

##Building
* Checkout the repository.
* Build with Visual Studio

##Screens
![](http://i.imgur.com/2cFfgm.png)
![](http://i.imgur.com/Msh72m.jpg)
![](http://i.imgur.com/223Nrm.png)
![](http://i.imgur.com/L5SVKm.png)
![](http://i.imgur.com/aqf2Bm.jpg)
![](http://i.imgur.com/b3PUQm.jpg)
![](http://i.imgur.com/PiBRQm.png)
![](http://i.imgur.com/5s7zfm.png)
![](http://i.imgur.com/KYtMXm.png)
![](http://i.imgur.com/eZesXm.jpg)